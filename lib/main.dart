import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Widget ThisSchool = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        /*1*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*2*/
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'Burapha University',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              'Seansuk Mueang Chon Buri',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ],
        ),
      ),
      /*3*/
      Image.asset(
        'images/buu.png',
        height: 70,
        fit: BoxFit.cover,
      ),
    ],
  ),
);

Widget SecSchool = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        /*1*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*2*/
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'Angsilapittayakom School',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              'Angsila Mueang Chon Buri',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ],
        ),
      ),
      /*3*/
      Image.asset(
        'images/asp.jpg',
        height: 70,
        fit: BoxFit.cover,
      ),
    ],
  ),
);

Widget name = Container(
  margin: const EdgeInsets.only(left: 40.0, top: 20.0),
  alignment: Alignment.topLeft,
  child: Text(
    "Name : Pawitporn Narathitiphan",
    style: TextStyle(
      fontSize: 20.0,
    ),
  ),
);

Widget studentID = Container(
  margin: const EdgeInsets.only(left: 40.0, top: 20.0),
  alignment: Alignment.topLeft,
  child: Text(
    "Student ID : 61160161 Gender : Male ",
    style: TextStyle(
      fontSize: 20.0,
    ),
  ),
);

Widget age = Container(
  margin: const EdgeInsets.only(left: 40.0, top: 20.0),
  alignment: Alignment.topLeft,
  child: Text(
    "I am 21 years old",
    style: TextStyle(
      fontSize: 20.0,
    ),
  ),
);

Widget study = Container(
  margin: const EdgeInsets.only(left: 40.0, top: 20.0),
  alignment: Alignment.topLeft,
  child: Text(
    "Computer Science , Faculty of Informatics Burapha University",
    style: TextStyle(
      fontSize: 20.0,
    ),
  ),
);

Widget MyHeader = Container(
  alignment: FractionalOffset.topCenter,
  child: Text(
    "My Resume",
    style: TextStyle(
        fontSize: 40.0,
        fontWeight: FontWeight.bold,
        color: Color.fromRGBO(0, 0, 255, 1)),
  ),
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Pawitporn Narathitiphan Resume'),
        ),
        body: Column(
          children: [
            MyHeader,
            Image.asset(
              'images/MyPic.jpg',
              height: 200,
              fit: BoxFit.cover,
            ),
            name,
            studentID,
            age,
            study,
            ThisSchool,
            SecSchool,
            Text(
              "My Skills",
              style: TextStyle(
                  fontSize: 20.00, color: Color.fromRGBO(0, 255, 0, 1)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _mySkill(Icons.widgets_sharp, 'HTML'),
                _mySkill(Icons.wysiwyg_sharp, 'CSS'),
                _mySkill(Icons.wine_bar, 'JAVA'),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Column _mySkill(IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: Colors.green[600]),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.green[600],
            ),
          ),
        ),
      ],
    );
  }
}
